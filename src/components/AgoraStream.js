import React, { useEffect, useState } from 'react';
import AgoraRTC from 'agora-rtc-sdk';
import CONFIG from '../appConfig';
import { startRecording, stopRecording } from './api';
import './canvas.css';
import '../assets/fonts/css/icons.css';

const agoraClient = AgoraRTC.createClient({ mode: 'live', codec: CONFIG.CODEC })

export const AgoraStream = (props) => {

  const streamConfig = {
    streamID: props.uid,
    audio: props.attendeeMode === 'host',
    video: props.attendeeMode === 'host',
    screen: false
  };

  const initRecordData = {
    resid: null,
    sid: null
  };

  const [localStream, setLocalStream] = useState(null);
  const [recordData, setRecordData] = useState(initRecordData);
  const [recordState, setRecordState] = useState(false);
  
  let stream = null;

  const localStreamInitCallback = (stream) => {
    console.log("Stream initialize:::");
    stream.setVideoProfile(CONFIG.VIDEO_PROFILE);

    if (props.attendeeMode === 'host') {
      agoraClient.setClientRole('host', () => console.warn("Client is host."));
      
      agoraClient.publish(stream, err => {
        console.log("Publish local stream error: " + err);
      })

      stream.play('ag-item');
    } else {
      agoraClient.setClientRole('audience', () => console.log("Client is audience."));
      agoraClient.setRemoteVideoStreamType(stream, 0);
    }
  }

  const clientJoinCallback = uid => {
    console.warn("User " + uid + " join channel successfully")
    stream = AgoraRTC.createStream(streamConfig);
    stream.init(() => localStreamInitCallback(stream), err => console.log("getUserMedia failed", err));
    
    setLocalStream(stream);
    
  };

  const clientInitCallback = () => {
    console.log("AgoraRTC client initialized")
    subscribeStreamEvents(props.hostId);
    agoraClient.join(props.token, props.channel, props.uid, props.channel, clientJoinCallback)
  };

  const handleRecord = async() => {
    if (props.attendeeMode !== 'host') { return; }
    
    if (!recordState ) {
      console.log('recording...');
      setRecordState(true);
      const response = await startRecording(props.channel, props.uid, 'live', props.token)
      setRecordData({
        resid: response.data?.resourceId,
        sid: response.data?.sid
      })
    } else {
      console.log('stop recording...');
      setRecordState(false);
      const response = await stopRecording(props.channel, props.uid, recordData.resid, recordData.sid);
      console.log(response);
    }  
  };

  const handleCamera = (e) => {
    e.currentTarget.classList.toggle('off');
    localStream.isVideoOn() 
      ? localStream.disableVideo() 
      : localStream.enableVideo()
  };
  
  const handleMic = (e) => {
    e.currentTarget.classList.toggle('off');
    localStream.isAudioOn() 
      ? localStream.disableAudio() 
      : localStream.enableAudio()
  };
  
  const handleExit = (e) => {
    agoraClient.unpublish(localStream);
    localStream.close();
    agoraClient.leave(
      () => { 
        console.log('Client succeed to leave.');
        props.streamExitHandler();
      } ,
      () => console.log('Client failed to leave.')
    );
  };

  const exitBtn = (
    <span
      onClick={handleExit}
      className={'ag-btn exitBtn'}
      title="Exit">
      <i className="ag-icon ag-icon-leave"></i>
    </span>
  );

  const hostControls = (
    <>
      <span
        onClick={handleCamera}
        className="ag-btn videoControlBtn"
        title="Enable/Disable Video">
        <i className="ag-icon ag-icon-camera"></i>
        <i className="ag-icon ag-icon-camera-off"></i>
      </span>

      <span
        onClick={handleMic}
        className="ag-btn audioControlBtn"
        title="Enable/Disable Audio">
        <i className="ag-icon ag-icon-mic"></i>
        <i className="ag-icon ag-icon-mic-off"></i>
      </span>

      <span
        onClick={handleRecord}
        className={!recordState ? 'ag-btn' : 'ag-btn recording'}
        title="Record">
        <i className="ag-icon ag-icon-screen-share"></i>
      </span>
    </>
  );

  useEffect(() => {
    agoraClient.init(CONFIG.APP_ID, clientInitCallback);
  }, [])

  return (
    <div id="ag-canvas">
      <div className="ag-btn-group">
        {exitBtn}
        { props.attendeeMode === 'host' && hostControls }
      </div>
      
      <section id="ag-item"></section>
    </div>
  );
};

const subscribeStreamEvents = () => {
  
  // host is added
  agoraClient.on('stream-added', evt => {
    let stream = evt.stream;
    let streamId = stream.getId();
    console.warn("New stream: " + streamId);
    
    agoraClient.subscribe(stream, { video: true, audio: true }, err => console.log("Subscribe stream failed", err));
    stream.play('ag-item');
  })

  // host leaving
  agoraClient.on('peer-leave', evt => {
    console.warn("Peer has left: " + evt.uid);
    
    let element = document.querySelector('ag-item');
    if (element) {
      element.parentNode.removeChild(element);
    } 
  })

  // event happens when audience is joining from user side 
  agoraClient.on('stream-subscribed', evt => {
    let stream = evt.stream;
    console.warn("Got stream-subscribed event");
    stream.play('ag-item');
  })

  agoraClient.on("stream-removed", evt => {
    let stream = evt.stream;
    console.warn("Stream removed: " + stream.getId());

    let element = document.querySelector('ag-item');
    if (element) {
      element.parentNode.removeChild(element);
    }
  })
}






