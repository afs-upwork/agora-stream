import React, { useState, useEffect } from 'react';
import { AgoraStream } from './components/AgoraStream';
import { generateToken } from './components/api';
import './App.css';

// Quick way to get supposedly dynamic parameters from query
const getParameterByName = (name, url = window.location.href) => {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

const App = () => {
  const channel = getParameterByName('channel');
  const uid = getParameterByName('uid');
  const attendeeMode = getParameterByName('attendee');
  const hasParams = channel && uid && attendeeMode;

  if (!hasParams){
    console.error('Missing one or more parameters. channel | uid | attendee')
  }
  
  const [token, setToken] = useState(null);

  const getToken = async() => {
    await generateToken(channel, uid, attendeeMode)
      .then((data) => {
        setToken(data.token);
      }
    );
  };

  useEffect(() => {
    if (!token && hasParams ) {
      getToken();  
    }
  });

  
  const streamExitHandler = () => {
    // need to implement in app
    console.log("streamExitHandler called. Navigate to another page");
  }

  return (
    <div className="App">
      <div className="wrapper">
        <div className="ag-header">Agora-Stream</div>
        <div className="ag-main">
          <div className="ag-container">
            { hasParams && token &&
              <AgoraStream
                channel={channel}
                attendeeMode={attendeeMode}
                token={token}
                uid={uid}
                streamExitHandler={streamExitHandler}
              />
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
