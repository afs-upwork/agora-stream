# Agora Stream

## How to test:
- Run the Backend API. Check `api/README.md`
- Run Frontend App
```
npm run start
```
- Open the following URLs in different browsers
-  For Host with UID: 1234
```
https://localhost:3000?channel=mychannel&uid=1234&attendee=host
```

-  For Audience with UID: 4567
```
https://localhost:3000?channel=mychannel&uid=4657&attendee=audience
```
## How to use the component:
```
<AgoraStream
  channel={channel}
  attendeeMode={attendeeMode}
  token={token}
  uid={uid}
  streamExitHandler={streamExitHandler}
/>
```
- `streamExitHandler` is the handler function that gets called when user leaves the channel
## Required files
- `src/appConfig.js`
- `src/components/AgoraStream.js`
- `src/components/api.js`

## Production URLS for testing
-  For Host with UID: 1234
```
https://stream-dot-agora-react.et.r.appspot.com/stream?channel=mychannel&uid=1234&attendee=host
```

-  For Audience with UID: 4567
```
https://stream-dot-agora-react.et.r.appspot.com/stream?channel=mychannel&uid=4657&attendee=audience
```